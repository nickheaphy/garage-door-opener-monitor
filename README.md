# ESP8266 Garage Door Opener

Work in progress.

## Door Monitor

The door monitor is based on a Wemos D1 mini ESP8266 board. This has a mercury switch connected between GND and D2 to detect the orientation of the board.

![Schematic](inc/Schematics/detector.png)

The ESP8266 connects to wifi every 5 min and uploaded the status of the garage door based on the orientation of the board (either open or closed)

The board and battery is attached to the garage door.

In the ESP8266 folder edit the `config_sample.h` and populate with your details. Save as `config.h`.

Compile `wemos_d1_detect.ino` and upload to the ESP8266.

There are the `.stl` files in the Case folder for a 3D printable case to mount the AA battery and Wemos D1 mini.

![Case](inc/Photos/case.jpg)

## Web Server PHP/MySQL

The `index.php` receives the status from the ESP8266 and stores this into a MySQL database.