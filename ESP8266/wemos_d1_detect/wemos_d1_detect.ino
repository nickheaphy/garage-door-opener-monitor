#include <ESP8266WiFi.h>

#include "config.h"

#define ORIENTATION_PIN 4 //pin 4 = D2

const char* ssid     = THE_SSID;
const char* password = THE_PW;
const char* host = THE_URL;
char* door_state = "UNDEFINED";

// sleep for this many seconds
const int sleepSeconds = 5*60;

void setup() {
  Serial.begin(115200);
  delay(10);

    // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  
  int i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    if (i < 20) {
      Serial.print(".");
    } else {
      //go to sleep. Wifi is borked
      Serial.println("Can't connect to Wifi. Deep sleeping...");
      ESP.deepSleep(sleepSeconds * 1000000);
    }
    delay(500);
    i++;
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  // setup the mercury switch
  pinMode(ORIENTATION_PIN, INPUT_PULLUP);
  // Connect D0 to RST to wake up
  pinMode(D0, WAKEUP_PULLUP);
}

void loop() {

  Serial.print("connecting to ");
  Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  // Check the state
  if (digitalRead(ORIENTATION_PIN) == 0) // get new state of door
    door_state = "0";
  else if (digitalRead(ORIENTATION_PIN) == 1)
    door_state = "1";

  Serial.print("Door State: ");
  Serial.println(door_state);

  // We now create a URI for the request
  String url = THE_PATH;
  url += "?open=";
  url += door_state;
  url += "&ss=";
  url += THE_SS;

  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  
  Serial.println();

  Serial.println("DEEP SLEEP");
  // convert to microseconds
  ESP.deepSleep(sleepSeconds * 1000000);
}
