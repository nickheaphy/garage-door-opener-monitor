<?php
include_once 'config.php';


// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
// Set autocommit to on
mysqli_autocommit($conn,TRUE);

if ($_GET['ss'] === $sharedsecret) {
    // setting data into the database
    if ($_GET['open'] === "1") {
        $open = 1;
    } elseif ($_GET['open'] === "0") {
        $open = 0;
    } else {
        $open = -1;
    }

    if ($open >= 0) {
        $sql = "INSERT INTO garagedoorstatus (open) VALUES ($open)";

        if (mysqli_query($conn, $sql)) {
            echo "New record created successfully - $open";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }

        //check how long the door has been open
        if ($open == 1) {
            //get the last time the door was seen as closed
            $sql = "SELECT TIMESTAMPDIFF(minute, (SELECT time FROM `garagedoorstatus` where open=0 order by id desc limit 1), time) as td FROM `garagedoorstatus` order by id desc limit 1";
            if ($result = mysqli_query($conn, $sql)) {
                $row = mysqli_fetch_row($result);
                if ($row[0] >= 15 && $row[0] <= 25) {
                    foreach ($notifyemails as &$email) {
                        mail($email,"Garage Door left Open","Garage Door should be closed");
                    }
                }
            }
        }

    } else {
        echo "Sorry, something wrong with data submitted";
    }

} else {
    // display data from the database
    // WTF is up with the server time?
    $sql = "SELECT id, open, DATE_ADD(time, INTERVAL 19 HOUR) as time, TIMESTAMPDIFF(SECOND, time, NOW()) as timediff FROM `garagedoorstatus` ORDER BY id DESC LIMIT 1";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            if (isset($_GET['mr'])) {
                // terse machine readable output
                echo $row["open"]. " " . $row["timediff"] . " " . $row["time"];
            } else {
                if ($row["open"] == "0") {
                    //echo "At " . $row["time"] . " the door was Closed.";
                    echo $row["timediff"] . " sec ago the door was seen as Closed<br/>";
                    echo "Last record id: " . $row["id"] . " - " . gmdate("H:i:s", $row["timediff"]) . " ago at " . $row["time"];
                } else {
                    echo $row["timediff"] . " sec ago the door was seen as Open<br/>";
                    echo "Last record id: " . $row["id"] . " - " . gmdate("H:i:s", $row["timediff"]) . " ago at " . $row["time"];
                }
            }
        }
    } else {
        echo "0 results";
    }
}

mysqli_close($conn);
?>