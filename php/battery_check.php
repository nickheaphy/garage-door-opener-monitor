<?php

// Check how long it was since the last update. Send alert if it has been a while

include_once 'config.php';

// how many seconds before deciding that batteries flat.
$errortime = 7200;

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT id, open, DATE_ADD(time, INTERVAL 19 HOUR) as time, TIMESTAMPDIFF(SECOND, time, NOW()) as timediff FROM `garagedoorstatus` ORDER BY id DESC LIMIT 1";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {

        if ($row['timediff'] < $errortime) {
            foreach ($notifyemails as &$email) {
                mail($email,"Garage Door Battery","Batteries need replacing.");
            }
            echo "<html><body>Battery Needs Replacing</body></html>";
        } else {
            echo "<html><body>Battery OK</body></html>";
        }
    }
}

mysqli_close($conn);
?>